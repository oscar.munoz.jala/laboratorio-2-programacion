# Hotel Comparison System

This program is designed to collect, organize and present a large amount of information about hotels in various countries, allowing users to make informed decisions when choosing accommodation for their upcoming trips.

## Justification

I took as a guide one of the most recognized applications with a large database of hotels, houses, apartments, etc...
This application is called Airbnb. The idea of this program is that the user can make a search within the countries where there is available information of hotels and this in turn can make a filter according to their needs.

## Input

- Add a new hotel to the list: searches.addHotel( Hotel(name, location, description, contact, priceNight, reviews, site) );
- Search option:
    - Enter by console the country where you want to search.
    - Enter by console the option of services that you want.
    - Enter by console the amount in USD of the maximum value that you would be willing to pay per night.

## Output

- List all hotels with the above characteristics in this format:

    - Name:
    - Location:
    - Description:
    - Contact:
    - Amenities:
    - Price per night:
    - Ratings:

## Workflow

```
---
title: Sample workflow
---
flowchart LR
start([start])
List <hotels> : list with the available hotels. 
Search a hotel:
	countries --> select the country in the console
	amenities --> select the aminity in the console
	priceNigth --> select the maximum price in the console
	
Filter the hotels:
	searchHotels ---> Received the country && aminity && price	
	      filter ---> List<hotels>
	        List<search> --> list with the hotels after the filter.
Show the search list:
	print --> Name, Location, Description, Contact, Amenities, 
	Price, Ratings, Site ([end])

```

## Diagram

![Diagram.png](Diagram.png)

## Execution

![Execution.png](Execution.png)

## Tests

![Tests.png](Tests.png)