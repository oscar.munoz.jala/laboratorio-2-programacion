package university.jala.academic;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private String name;
    private String location;
    private String description;
    private String contact;
    private double priceNight;
    private double reviews;
    private String site;
    private List<String> amenities;

    public Hotel(String name, String location, String description, String contact, double priceNight,double reviews, String site) {
        this.name = name;
        this.location = location;
        this.description = description;
        this.contact = contact;
        this.amenities = new ArrayList<>();
        this.priceNight = priceNight;
        this.reviews = reviews;
        this.site = site;
    }

    public String getName() {
        return name;
    }
    public String getLocation() {
        return location;
    }
    public String getDescription() { return description; }
    public String getContact() {
        return contact;
    }
    public List<String> getAmenities() {
        return amenities;
    }
    public double getPriceNight() {
        return priceNight;
    }
    public double getReviews() { return reviews; }
    public String getSite() {
        return site;
    }
    public void addAmenities(String amenity) { amenities.add(amenity); }

}
