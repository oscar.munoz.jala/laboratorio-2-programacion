package university.jala.academic;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Examples of hotels
        Hotel hotel1 = new Hotel(
                "Luxurious 2500m2 Super TopSpot in Anapoima", "Colombia",
                "Great two story house in one of the best areas of Mesa de Yeguas.", "+573100000000",
                100, 4.87,"https://www.airbnb.com/rooms/831905093374954028");
        hotel1.addAmenities("Pool");
        hotel1.addAmenities("WiFi");

        Hotel hotel2 = new Hotel(
                "Deluxe TopSpot in Mesa de Yeguas-Anapoima! M77", "Colombia",
                "Great two story house in one of the best areas of Mesa de Yeguas.", "+573110000000",
                40, 4.88,"https://www.airbnb.com/rooms/943050682529104852");
        hotel2.addAmenities("WiFi");

        Hotel hotel3 = new Hotel(
                "Inti Illimani Lodge Suite Inti Isla del Sol", "Bolivia",
                "Disconnect from your worries at this spacious and serene space on Isla del Sol Bolivia.", "+59131000000",
                100,4.92, "https://www.airbnb.com/rooms/871587313134192305");
        hotel3.addAmenities("Parking");

        Hotel hotel4 = new Hotel(
                "'La Biosfera' For the world and you.", "Bolivia",
                "The architecture is unique, integrated into nature with animals, flames, horse, geese, peacock, fish in the lagoon.", "+59131100000",
                50,4.92, "https://www.airbnb.com/rooms/7850586");
        hotel4.addAmenities("Pool");
        hotel4.addAmenities("Parking");

        Hotel hotel5 = new Hotel(
                "Cozy Alpine cabin in the woods close to Zacatlan", "Mexico",
                "Elevated over the Valle de Piedras Encimadas, our chalets give you unparalleled views, giving you a feeling of freedom and intimacy.", "+5231000000",
                80,4.92, "https://www.airbnb.com/rooms/52797799");
        hotel5.addAmenities("Parking");

        Hotel hotel6 = new Hotel(
                "Casa WO- Oasis at Mexico's Chillest Surf Town", "Mexico",
                "A modern oasis and architectural marvel in the Mexican state of Oaxaca.", "+5231100000",
                50,4.96, "https://www.airbnb.com/rooms/7850586");
        hotel6.addAmenities("Pool");
        hotel6.addAmenities("WiFi");

        //Hotels added in the search
        Searches searches = new Searches();
        searches.addHotel(hotel1);
        searches.addHotel(hotel2);
        searches.addHotel(hotel3);
        searches.addHotel(hotel4);
        searches.addHotel(hotel5);
        searches.addHotel(hotel6);


        Scanner scanner = new Scanner(System.in);
        String newSearch = "yes";

        System.out.println("------------------------------");
        System.out.println("-       SEARCH A HOTEL       -");
        System.out.println("------------------------------");

        //New search

        do {
            searches.searches.clear();
            String location = "";
            String amenity = "";
            double priceNight = 0.0;

            while (location.isEmpty()) {
                try {
                    System.out.println("\nCountries:\n");
                    System.out.println("1. Colombia");
                    System.out.println("2. Mexico");
                    System.out.println("3. Bolivia\n");
                    System.out.print("Enter the number of location: ");
                    int optionLocation = scanner.nextInt();
                    location = getLocation(optionLocation);

                } catch (InputMismatchException e) {
                    System.out.println("Error: Enter a valid number.");
                }
            }

            while (amenity.isEmpty()) {
                try {
                    System.out.println("\nServices:\n");
                    System.out.println("1. WiFi");
                    System.out.println("2. Pool");
                    System.out.println("3. Parking\n");
                    System.out.print("Enter the number of amenity that it is necessary for you: ");
                    int optionAmenity = scanner.nextInt();
                    amenity = getAmenity(optionAmenity);

                } catch (InputMismatchException e) {
                    System.out.println("Error: Enter a valid number.");
                    scanner.nextLine();
                }
            }
            while (priceNight == 0.0) {
                try {
                    System.out.print("Enter the maximum price per night: ");
                    priceNight = scanner.nextDouble();

                } catch (InputMismatchException e) {
                    System.out.println("Error: Enter a valid amount.");
                    scanner.nextLine();
                }
            }

            List<Hotel> resultSearch = searches.searchHotels(location, priceNight, amenity);

            if (!resultSearch.isEmpty()) {

                System.out.println("------------------------------");
                System.out.println("-       SEARCH RESULTS       -");
                System.out.println("------------------------------\n");


                for (Hotel search : resultSearch) {
                    System.out.println("Name: " + search.getName() + "\n" + "Location: " + search.getLocation() + "\n" + "Description: " + search.getDescription() + "\n" +
                            "Contact: " + search.getContact() + "\n" + "Amenities: " + search.getAmenities() + "\n" + "Price per night: $" + search.getPriceNight() + " USD\n" +
                            "Ratings: ★" + search.getReviews() + "\n" + "Site: " + search.getSite() + "\n" + "\n------------------------------\n");
                }
            }
            else {
                System.out.println("------------------------------");
                System.out.println("-          NO RESULTS        -");
                System.out.println("------------------------------\n");
            }

            System.out.print("Do you want to make a new search? (yes/no): ");
            newSearch = scanner.next();
            scanner.reset();

        } while (newSearch.equalsIgnoreCase("yes"));

        System.out.println("------------------------------");
        System.out.println("-      SEARCH COMPLETED      -");
        System.out.println("------------------------------");
    }
    //Countries options
    public static String getLocation ( int option){
        String location = "";
        switch (option) {
            case 1:
                location = "Colombia";
                break;
            case 2:
                location = "Mexico";
                break;
            case 3:
                location = "Bolivia";
                break;
            default:
                System.out.println("Error: Enter a valid option.");
                break;
        }
        return location;
    }
    //Amenities options
    public static String getAmenity ( int option){
        String amenity = "";
        switch (option) {
            case 1:
                amenity = "WiFi";
                break;
            case 2:
                amenity = "Pool";
                break;
            case 3:
                amenity = "Parking";
                break;
            default:
                System.out.println("Error: Enter a valid option.");
                break;
        }
        return amenity;
    }

}
