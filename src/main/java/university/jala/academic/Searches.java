package university.jala.academic;

import java.util.ArrayList;
import java.util.List;

public class Searches {

    List<Hotel> hotels; //All available hotels
    List<Hotel> searches = new ArrayList<>();

    public Searches() {
        this.hotels = new ArrayList<>();
    }

    public void addHotel(Hotel hotel) {
        hotels.add(hotel);
    }
    //Method to find the hotels with the characteristics.
    public List<Hotel> searchHotels(String location, double priceNight, String amenity) {

        for (Hotel hotel : hotels) {
            if (hotel.getLocation().equalsIgnoreCase(location) && hotel.getPriceNight() <= priceNight) {
                for (int i = 0; i < hotel.getAmenities().size(); i++) {
                    if (hotel.getAmenities().get(i).equalsIgnoreCase(amenity)) {
                        searches.add(hotel);
                    }
                }
            }
        }
        return searches;
    }
}
