package university.jala.academic;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class HotelTest {

    @Test
    public void createNewHotel(){

        Hotel hotel = new Hotel("Name 1","Location 1", "Description 1", "00000000",10,0.0,"Site 1");
        assertEquals("Name 1",hotel.getName());

    }

    @Test
    public void addHotelToSearches(){

        Searches searches = new Searches();
        Hotel hotel = new Hotel("Name 1","Location 1", "Description 1", "00000000",10,0.0,"Site 1");
        searches.hotels.add(hotel);
        assertEquals(1,searches.hotels.size());

    }

    @Test
    public void filterHotels(){

        Searches searches = new Searches();
        Hotel hotel = new Hotel("Name 1","Location 1", "Description 1", "00000000",10,0.0,"Site 1");
        searches.hotels.add(hotel);
        hotel.addAmenities("Amenity 1");

        List<Hotel> resultSearch = searches.searchHotels("Location 1",10,"Amenity 1");;
        for (Hotel search : resultSearch) {
            assertEquals("Name 1",search.getName());
        }
    }

}

